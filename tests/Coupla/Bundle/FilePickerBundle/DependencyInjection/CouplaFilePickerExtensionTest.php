<?php
/**
 * Copyright (c) 2013 Adam L. Englander
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace Coupla\Bundle\FilePickerBundle\DependencyInjection;

use Phake;

/**
 * @author Adam L. Englander <adam.l.englander@coupla.co>
 *
 * Messaging extension unit tests
 */
class CouplaFilePickerExtensionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var string
     */
    private $configPath;

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerBuilder
     * @Mock
     */
    private $container;

    /**
     * @var CouplaMessagingExtension
     */
    private $extension;

    /**
     * @var \Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface
     * @Mock
     */
    private $parameterBag;

    protected function setUp()
    {
        parent::setUp();
        Phake::initAnnotations($this);
        $this->extension = new CouplaFilePickerExtension();
        $this->configPath = realpath(__DIR__ . '/../../../../../src/Coupla/Bundle/FilePickerBundle/Resources/config') . '/';
        Phake::when($this->container)
            ->getParameterBag()
            ->thenReturn($this->parameterBag);
    }

    public function testLoadLoadsServicesXML()
    {
        $configs = array();
        $this->extension->load($configs, $this->container);

        /** @var \Symfony\Component\Config\Resource\FileResource $resource  */
        $resource = null;
        Phake::verify($this->container)
            ->addResource(Phake::capture($resource));
        $expected = $this->configPath . 'services.xml';
        $this->assertEquals($expected, $resource->getResource());
    }

    public function testLoadNoApiKeyDoesntSetApiKey()
    {
        $configs = array();
        $this->extension->load($configs, $this->container);
        Phake::verify($this->container, Phake::never())
            ->setParameter('coupla_file_picker.api_key', $this->anything());
    }

    public function testLoadWithApiKeySetsApiKey()
    {
        $expected = 'parameter value';
        $configs = array(array('api_key' => $expected));
        $this->extension->load($configs, $this->container);
        Phake::verify($this->container)
            ->setParameter('coupla_file_picker.api_key', $expected);
    }
}
