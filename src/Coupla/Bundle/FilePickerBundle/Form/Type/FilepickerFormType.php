<?php
/**
 * Copyright (c) 2013 Adam L. Englander
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace Coupla\Bundle\FilePickerBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @author Adam L. Englander <adam.l.englander@coupla.co>
 *
 * Form type for Filepicker.io pick widget
 *
 * @TODO Add self signing mechanism
 */
class FilepickerFormType extends AbstractType
{
    /**
     * @var
     */
    private $apiKey;

    /**
     * @var string
     */
    private $services;

    /**
     * @param string $apiKey Filepicker.io API key for this application
     */
    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
        $this->services = implode(
            ',',
            array(
                'BOX',
                'COMPUTER',
                'DROPBOX',
                'EVERNOTE',
                'FACEBOOK',
                'FLICKR',
                'FTP',
                'GITHUB',
                'GOOGLE_DRIVE',
                'PICASA',
                'WEBDAV',
                'GMAIL',
                'IMAGE_SEARCH',
                'INSTAGRAM',
                'URL',
                'VIDEO',
                'WEBCAM'
            )
        );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'filepicker';
    }

    public function getParent()
    {
        return 'text';
    }   

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data-fp-apikey' => $this->apiKey,
                'data-fp-services' => $this->services
            )
        );
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $attr = $view->vars['attr'];
        foreach ($options as $key => $value) {
            if ($this->isFilePickerOption($key) && !array_key_exists($key, $attr)) {
                $view->setAttribute($key, $value);
            }
        }

    }

    private function isFilePickerOption($key)
    {
        return preg_match('/^data\-fp\-/', $key) == 1;
    }
}
