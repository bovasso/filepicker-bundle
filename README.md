Coupla FilePicker Bundle
========================

The Coupla FilePicker bundle provides simplified integration with Filepicker.io's JavaScript API for Symfony 2 users.

Documentation
-------------

The documentation is provided at the project wiki: https://bitbucket.org/coupla/filepicker-bundle/wiki
